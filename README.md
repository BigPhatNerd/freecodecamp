# [freecodecamp](https://www.freecodecamp.org/)

###### *Wilson Horrell is a full-stack web developer and technology enthusiast. You can find more information about him through his [portfolio](https://wilsonhorrell.netlify.com/) or contact him directly at the email address wilsonhorrell@gmail.com*

Solutions to some of the problems that were presented in the Javascript Algorithms and Data Structures Certification.

<img width="832" alt="Screen Shot 2019-07-11 at 2 34 54 PM" src="https://user-images.githubusercontent.com/44242436/61079747-15cd6300-a3e9-11e9-8c82-706cac1d2516.png">
