function findLongestWordLength(str) {
   let answer = (str.split(' ').reduce(function(a,b) {return a.length > b.length ? a : b ;}));
   return answer.length;
}

findLongestWordLength("The quick brown fox jumped over the lazy dog");