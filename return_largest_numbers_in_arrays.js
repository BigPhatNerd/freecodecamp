function largestOfFour(arr) {
  let largest_of_each = [];
  for (var i = 0; i < arr.length; i++) {
  	var largestNumber = arr[i][0];
  	for (var sb = 1; sb < arr[i].length; sb++) {
  		if (arr[i][sb] > largestNumber) {
  			largestNumber = arr[i][sb];
  		}
  	}
  	largest_of_each[i] = largestNumber;
  }
   return largest_of_each;
}

largestOfFour([[4, 5, 1, 3], [13, 27, 18, 26], [32, 35, 37, 39], [1000, 1001, 857, 1]]);